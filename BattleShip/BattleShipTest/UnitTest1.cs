using BattleShip;
using Newtonsoft.Json;
using NUnit.Framework;
using System;

namespace BattleShipTest
{
    public class Tests
    {
        public User user;
        public BoardView boardView;

        [SetUp]
        public void Setup()
        {
            user = new User();
            boardView = new BoardView();
            Const.BotField = new int[10,10];
            Const.BotField[3, 3] = 1;
            Const.BotField[3, 4] = 1;
            Const.BotField[6, 8] = 0;
            var s = JsonConvert.SerializeObject(boardView);
            Console.WriteLine(s);

        }

        [Test]
        public void TestHit()
        {
            Assert.AreEqual(Const.BotField[3, 3] = 2,user.Hit(3,3));
            Assert.AreEqual(false,user.Hit(6,8));
        }
    }
}