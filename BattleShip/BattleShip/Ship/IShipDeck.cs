﻿

namespace BattleShip.Ship
{
    interface IShipDeck
    {
        public void GetShipSize();
        public void ChooseRowDeck(int x, int y);
        public void ChooseColumnDeck(int y, int x);
    }
}
