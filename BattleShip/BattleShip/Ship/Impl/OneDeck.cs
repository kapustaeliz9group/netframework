﻿using System;


namespace BattleShip.Ship.Impl
{
    class OneDeck 
    {
        public void SetShipSize()
        {
            var random = new Random();
            var x = random.Next(10);
            var y = random.Next(10);
            for (int i = y - 1; i < y + 2; i++)
            {
                if (i < 0)
                {
                    i++;
                }
                if (i > 9)
                {
                    break;
                }
                for (int j = x - 1; j < x + 2; j++)
                {
                    if (j < 0)
                    {
                        j++;
                    }
                    if (j > 9)
                    {
                        break;
                    }
                    if (Const.BotField[i, j] != 0)
                    {
                        return;
                    }
                }
            }
            Const.BotField[y, x] = 1;
            Const.Number++;
        }
    }
}
