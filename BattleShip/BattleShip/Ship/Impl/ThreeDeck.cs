﻿using System;

namespace BattleShip.Ship.Impl
{
    class ThreeDeck : IShipDeck
    {
        public void ChooseColumnDeck(int y, int x)
        {
            for (int i = y - 1; i < y + 4; i++)
            {
                if (i < 0)
                {
                    i++;
                }
                if (i > 9)
                {
                    break;
                }
                for (int j = x - 1; j < x + 2; j++)
                {
                    if (j < 0)
                    {
                        j++;
                    }
                    if (j > 9)
                    {
                        break;
                    }
                    {
                        if (Const.BotField[i, j] != 0)
                        {
                            return;
                        }
                    }
                }
            }
            for (int i = y; i < y + 3; i++)
            {
                Const.BotField[i, x] = 1;
            }
            Const.Number++;
            return;
        }

        public void ChooseRowDeck(int x, int y)
        {
            for (int i = y - 1; i < y + 2; i++)
            {
                if (i < 0)
                {
                    i++;
                }
                if (i > 9)
                {
                    break;
                }
                for (int j = x - 1; j < x + 4; j++)
                {
                    if (j < 0)
                    {
                        j++;
                    }
                    if (j > 9)
                    {
                        break;
                    }
                    if (Const.BotField[i, j] != 0)
                    {
                        return;
                    }
                }
            }
            for (int j = x; j < x + 3; j++)
            {
                Const.BotField[y, j] = 1;
            }
            Const.Number++;
            return;
        }

        public void GetShipSize()
        {
            var random = new Random();
            var x = random.Next(10);
            var y = random.Next(10);
            if (y > 6)
            {
                x = random.Next(7);
                ChooseRowDeck(x, y);
            }
            if (x > 6)
            {
                y = random.Next(7);
                ChooseColumnDeck(y, x);
            }
            int k = random.Next(1);
            if (k == 0)
            {
                for (int i = y - 1; i < y + 4; i++)
                {
                    if (i < 0)
                    {
                        i++;
                    }
                    if (i > 9)
                    {
                        break;
                    }
                    for (int j = x - 1; j < x + 2; j++)
                    {
                        if (j < 0)
                        {
                            j++;
                        }
                        if (j > 9)
                        {
                            break;
                        }
                        if (Const.BotField[i, j] != 0)
                        {
                            return;
                        }
                    }
                }
                for (int i = y; i < y + 3; i++)
                {
                    Const.BotField[i, x] = 1;
                }
                Const.Number++;
            }
            else
            {
                for (int i = y - 1; i < y + 2; i++)
                {
                    if (i < 0)
                    {
                        i++;
                    }
                    if (i > 9)
                    {
                        break;
                    }
                    for (int j = x - 1; j < x + 4; j++)
                    {
                        if (j < 0)
                        {
                            j = 0;
                        }
                        if (j > 9)
                        {
                            break;
                        }
                        if (Const.BotField[i, j] != 0)
                        {
                            return;
                        }
                    }
                }
                for (int j = x; j < x + 3; j++)
                {
                    Const.BotField[y, j] = 1;
                }
                Const.Number++;
            }
        }
    }
}
