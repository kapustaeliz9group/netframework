﻿using System;

namespace BattleShip.Ship.Impl
{
    class FourDeck : IShipDeck
    {
        public void ChooseColumnDeck(int y, int x)
        {
            for (int i = y; i < y + 4; i++)
            {
                Const.BotField[i, x] = 1;
            }
        }

        public void ChooseRowDeck(int x, int y)
        {
            for (int j = x; j < x + 4; j++)
            {
                Const.BotField[y, j] = 1;
            }
        }

        public void GetShipSize()
        {
            var random = new Random();
            int x = random.Next(10);
            int y = random.Next(10);
            if (x > 5)
            {
                y = random.Next(5);
                ChooseColumnDeck(y, x);
                return;
            }
            if (y > 5)
            {
                x = random.Next(5);
                ChooseRowDeck(x, y);
                return;
            }
            int k = random.Next(1);
            if (k == 0)
            {
                for (int i = y; i < y + 4; i++)
                {
                    Const.BotField[i, x] = 1;
                }
            }
            else
            {
                for (int j = x; j < x + 4; j++)
                {
                    Const.BotField[y, j] = 1;
                }
            }
        }
    }
}
