﻿using BattleShip.Ship.Impl;
using System;

namespace BattleShip
{
    public class BoardView
    {
        private FourDeck fourDeck = new FourDeck();
        private ThreeDeck threeDeck = new ThreeDeck();
        private TwoDeck twoDeck = new TwoDeck();
        private OneDeck oneDeck = new OneDeck();

        public BoardView()
        {
            fourDeck.GetShipSize();
            while (Const.Number < Const.ThreeDeck)
            {
                threeDeck.GetShipSize();
            }
            Const.Number = 0;
            while (Const.Number < Const.TwoDeck)
            {
                twoDeck.GetShipSize();
            }
            Const.Number = 0;
            while (Const.Number < Const.OneDeck)
            {
                oneDeck.SetShipSize();
            }
        }

        public void Output()
        {
            if (Const.indent > 20)
            {
                Const.indent = 2;
                Console.Clear();
            }
            OutputLetter();
            OutPutNumber();
            FillFields();
        }

        private void OutputLetter()
        {
            for (var i = 0; i < 10; i++)
            {
                Console.SetCursorPosition(2 * i + 3, 0);
                Console.Write(Const.str1[i]);
            }
        }

        private void OutPutNumber()
        {
            for (var i = 0; i < 10; i++)
            {
                Console.SetCursorPosition(0, i + 1);
                Console.Write(Const.str2[i]);
            }
        }
        private void FillFields()
        {
            for (var i = 0; i < 10; i++)
            {
                Console.SetCursorPosition(2, i + 1);
                Console.Write("| ");
                for (var j = 0; j < 10; j++)
                {
                    Console.SetCursorPosition(2 * j + 3, i + 1);
                    FillingCharactersFields(Const.BotField[i, j]);
                }
            }
        }

        public void FillingCharactersFields(int value)
        {
            switch (value)
            {
                case 0:
                    Console.Write('+');
                    break;
                case 1:
                    Console.Write('\u25A0');
                    break;
                case 2:
                    Console.Write('X');
                    break;
                case 3:
                    Console.Write('O');
                    break;
            }
        }
    }
}
