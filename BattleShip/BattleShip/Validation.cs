﻿using System;


namespace BattleShip
{
    class Validation
    {
        public void PrintHit()
        {
            Console.SetCursorPosition(30, 0);
            Console.Write("You can't shoot this cage");
            Console.SetCursorPosition(30, 4);
            Console.WriteLine();
            Const.Step--;
        }

        public bool HitShip(int row, int column)
        {
            if (row == 0)
            {
                return ZeroRow(row, column);
            }
            if (row == 9)
            {
                return NineRow(row, column);
            }
            if (column == 0)
            {
                return ZeroColumn(row, column);
            }
            if (column == 9)
            {
                return NineColumn(row, column);
            }

            if ((Const.BotField[row - 1, column] == 1) || (Const.BotField[row + 1, column] == 1) ||
                     (Const.BotField[row, column + 1] == 1) || (Const.BotField[row, column - 1] == 1))
            {
                Hurt(row, column);
                return true;
            }
                Drop(row, column);
                return true;
        }


        private bool ZeroRow(int row, int column)
        {
            if ((Const.BotField[row + 1, column] == 1) ||
                (column < 9 && (Const.BotField[row, column + 1] == 1)) || (column > 0 && (Const.BotField[row, column - 1] == 1)))
            {
                Hurt(row, column);
                return true;
            }
                Drop(row, column);
                return true;
        }

        private bool NineRow(int row, int column)
        {
            if ((row > 0 && (Const.BotField[row - 1, column] == 1)) ||
                (column < 9 && (Const.BotField[row, column + 1] == 1)) || (column > 0 && (Const.BotField[row, column - 1] == 1)))
            {
                Hurt(row, column);
                return true;
            }
                Drop(row, column);
                return true;
        }

        private bool ZeroColumn(int row, int column)
        {
            if ((row < 9 && (Const.BotField[row + 1, column] == 1)) ||
                (column < 9 && (Const.BotField[row, column + 1] == 1)) || (row > 0 && (Const.BotField[row - 1, column] == 1)))
            {
                Hurt(row, column);
                return true;
            }
                Drop(row, column);
                return true;
        }

        private bool NineColumn(int row, int column)
        {
            if ((row < 9 && (Const.BotField[row + 1, column] == 1)) ||
                (column > 0 && (Const.BotField[row, column - 1] == 1)) || (row > 0 && (Const.BotField[row - 1, column] == 1)))
            {
                Hurt(row, column);
                return true;
            }
                Drop(row, column);
                return true;
        }


        public void Miss(int row, int column)
        {
            Const.BotField[row, column] = 3;
            Const.Field1[row, column] = 3;
            Console.SetCursorPosition(30, 0);
            Console.Write("Miss!                     ");
        }

        private void Hurt(int row, int column)
        {
            Const.BotField[row, column] = 2;
            Const.Field1[row, column] = 2;
            Stroke(Const.BotField, row, column);
            Console.SetCursorPosition(30, 0);
            Console.Write("Hit!                          ");
        }

        private void Drop(int row, int column)
        {
            Const.BotField[row, column] = 2;
            Const.Field1[row, column] = 2;
            Stroke(Const.BotField, row, column);
            Console.SetCursorPosition(30, 0);
            Console.Write("Drop!                             ");
        }

        void Stroke(int[,] Field, int i, int j)
        {
            int Long = 1;
            int x = j;
            int y = i;
            for (int k = 1; k < 4; k++)
            {
                try
                {
                    if (Field[i - k, j] == 2)
                    {
                        Long++;
                        y--;
                    }
                    if (Field[i - k, j] == 1)
                    {
                        return;
                    }
                    if (Field[i - k, j] == 0 || Field[i - k, j] == 3)
                    {
                        break;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    break;
                }
            }
            for (int k = 1; k < 4; k++)
            {
                try
                {
                    if (Field[i + k, j] == 2)
                    {
                        Long++;
                    }
                    if (Field[i + k, j] == 1)
                    {
                        return;
                    }
                    if (Field[i + k, j] == 0 || Field[i + k, j] == 3)
                    {
                        break;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    break;
                }
            }
            if (Long > 1)
            {
                for (int k = y - 1; k < y + Long + 1 && k < 10; k++)
                {
                    if (k < 0)
                    {
                        k++;
                    }
                    for (int l = x - 1; l < x + 2 && l < 10; l++)
                    {
                        if (l < 0)
                        {
                            l++;
                        }
                        if (Field[k, l] != 2)
                        {
                            Field[k, l] = 3;
                            Const.Field1[k, l] = 3;
                        }
                    }
                }
                return;
            }

            for (int k = 1; k < 4; k++)
            {
                try
                {
                    if (Field[i, j - k] == 2)
                    {
                        Long++;
                        x--;
                    }
                    if (Field[i, j - k] == 1)
                    {
                        return;
                    }
                    if (Field[i, j - k] == 0 || Field[i, j - k] == 3)
                    {
                        break;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    break;
                }
            }
            for (int k = 1; k < 4; k++)
            {
                try
                {
                    if (Field[i, j + k] == 2)
                    {
                        Long++;
                    }
                    if (Field[i, j + k] == 1)
                    {
                        return;
                    }
                    if (Field[i, j + k] == 0 || Field[i, j + k] == 3)
                    {
                        break;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    break;
                }
            }
            if (Long > 1)
            {
                for (int l = y - 1; l < y + 2 && l < 10; l++)
                {
                    for (int k = x - 1; k < x + Long + 1 && k < 10; k++)
                    {
                        if (k < 0)
                        {
                            k++;
                        }
                        if (l < 0)
                        {
                            l++;
                        }
                        if (Field[l, k] != 2)
                        {
                            Field[l, k] = 3;
                            Const.Field1[l, k] = 3;
                        }
                    }
                }
                return;
            }

            if (Long == 1)
            {
                for (int k = y - 1; k < y + 2 && k < 10; k++)
                {
                    if (k < 0)
                    {
                        k = 0;
                    }
                    for (int l = x - 1; l < x + 2 && l < 10; l++)
                    {
                        if (l < 0)
                        {
                            l = 0;
                        }
                        if (Field[k, l] != 2)
                        {
                            Field[k, l] = 3;
                            Const.Field1[k, l] = 3;
                        }
                    }
                }
            }
        }
    }
}
