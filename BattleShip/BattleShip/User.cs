﻿using System;

namespace BattleShip
{
    public class User 
    {
        public static int[] Letter = new int[101];
        public static int[] Index = new int[101];
        public static int points = 0;
        Validation validation = new Validation();

        public void Strike()
        {
            if (Win())
            {
                return;
            }
            Console.SetCursorPosition(30, Const.indent++);
            Console.WriteLine("Shot №: " + ++Const.Step);
            ChooseLetter();
            int stepNumber = Convert.ToInt32(Console.ReadLine());
            if (stepNumber > 10 || stepNumber < 1)
            {
                Console.SetCursorPosition(30, 0);
                Console.WriteLine("Invalid input                      ");
            }
            else
            {
                Index[Const.Step] = stepNumber - 1;
                if (Hit(Index[Const.Step], Letter[Const.Step]))
                {
                    points++;
                    Strike();
                }
            }
        }
        
        private void ChooseLetter()
        {
            bool letterIsValid = true;
            while (letterIsValid)
            {
                Console.SetCursorPosition(30, Const.indent++);
                Console.Write("Your shot: ");
                switch (Console.Read())
                {
                    case 'a':
                        Letter[Const.Step] = 0;
                        letterIsValid = false;
                        break;
                    case 'b':
                        Letter[Const.Step] = 1;
                        letterIsValid = false;
                        break;
                    case 'c':
                        Letter[Const.Step] = 2;
                        letterIsValid = false;
                        break;
                    case 'd':
                        Letter[Const.Step] = 3;
                        letterIsValid = false;
                        break;
                    case 'e':
                        Letter[Const.Step] = 4;
                        letterIsValid = false;
                        break;
                    case 'f':
                        Letter[Const.Step] = 5;
                        letterIsValid = false;
                        break;
                    case 'g':
                        Letter[Const.Step] = 6;
                        letterIsValid = false;
                        break;
                    case 'h':
                        Letter[Const.Step] = 7;
                        letterIsValid = false;
                        break;
                    case 'i':
                        Letter[Const.Step] = 8;
                        letterIsValid = false;
                        break;
                    case 'j':
                        Letter[Const.Step] = 9;
                        letterIsValid = false;
                        break;
                    default:
                        break;
                }
            }
        }

        public bool Hit(int row, int column)
        {
            if (Const.BotField[row, column] == 0)
            {
                validation.Miss(row, column);
                return false;
            }

            if (Const.BotField[row, column] == 1)
            {
                return validation.HitShip(row, column);
            }
            validation.PrintHit();
            return true;
        }

        
        public bool Win()
        {
            if (points == 20)
            {
                Console.SetCursorPosition(30, 0);
                Console.Clear();
                Console.Write("You won!                         ");
                return true;
            }
            return false;
        }
    }
}
