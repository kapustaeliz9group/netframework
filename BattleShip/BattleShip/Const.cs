﻿using System.Text;

namespace BattleShip
{
    public class Const
    {
        public  const int FourDeck = 1;
        public const int ThreeDeck = 2;
        public const int TwoDeck = 3;
        public const int OneDeck = 4;
        public static readonly string[] str1 = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j" };
        public static readonly string[] str2 = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
        public static int indent = 2;
        public static int Number = 0;
        public static int Step = new int();
        public static int[,] Field1 = new int[10, 10]; // 0 - empty, 1 - ship, 2 - hit by ship, 3 - miss.
        public static int[,] BotField = new int[10, 10];
        public static int[,] UserField = new int[10, 10];
        public static StringBuilder str = new StringBuilder();
    }
}
