﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalculatorOOP
{
    class MenuNumbers : Const
    {
        public List<string> history = new List<string>();
        public StringBuilder stringBuilder = new StringBuilder();
        public double Result { get; set; }
        public double FirstNumber { get; set; }
        public double SecondNumber { get; set; }
        public string Answer { get; set; }

        private Validation validation;
        private CalculatorOperation calculatorOperation;


        public MenuNumbers()
        {
            validation = new Validation();
            calculatorOperation = new CalculatorOperation();
        }

        public string GetMenuCalculate()
        {
            Console.WriteLine("Choose the operation: \n1.Add\n2.Substract\n3.Multiply\n4.Divide");
            string operation = Console.ReadLine();
            return operation;
        }

        public void Calculate(string operation)
        {
            if (Result == 0)
            {
                Console.WriteLine("Enter the firstNumber");
                FirstNumber = validation.CheckNumberDouble(FirstNumber);
            }
            else
            {
                FirstNumber = Result;
            }
            Console.WriteLine("Enter the secondNumber");
            SecondNumber = validation.CheckNumberDouble(SecondNumber);
            this.MenuOperationNumbers(operation);
        }

        private void MenuOperationNumbers(string operation)
        {
            stringBuilder.Append(Convert.ToString(FirstNumber));
            history.Add(Convert.ToString(FirstNumber));

            switch (operation)
            {
                case ONE:
                    Result = calculatorOperation.Add(FirstNumber, SecondNumber);
                    stringBuilder.Append(ADD);
                    break;
                case TWO:
                    Result = calculatorOperation.Substract(FirstNumber, SecondNumber);
                    stringBuilder.Append(SUBSTRACT);
                    break;
                case THREE:
                    Result = calculatorOperation.Multiply(FirstNumber, SecondNumber);
                    stringBuilder.Append(MULTIPLY);
                    break;
                case FOUR:
                    try
                    {
                        Result = calculatorOperation.Divide(FirstNumber, SecondNumber);
                    }
                    catch (ArithmeticException e)
                    {
                        Console.WriteLine($"Exception: {e.Message}");
                        GetMenuCalculate();
                    }
                    stringBuilder.Append(Convert.ToString(DIVIDE));
                    break;
            }
            Console.WriteLine($"Result: {Result}");

            stringBuilder.Append(Convert.ToString(SecondNumber));
            stringBuilder.Append(EQUAIS);
            stringBuilder.Append(Convert.ToString(Result));
            history.Add(stringBuilder + "\n");
            stringBuilder.Clear();
            OperationAnswerHistory();
        }

        public void OperationNumber()
        {
            string operation;
            while (true)
            {
                operation = this.GetMenuCalculate();
                if (validation.ValidOperation(operation, ValidInputOperation))
                {
                    this.Calculate(operation);
                }
            }
        }

        public string WantResetZero()
        {
            Console.WriteLine("Do you want to continue to work with the result? yes/no");
            string answer = Console.ReadLine();
            return answer;
        }

        public void OperationAnswerZero()
        {
            Answer = WantResetZero();
            ResetZero(Answer);
        }

        public void ResetZero(string answer)
        {
            switch (answer)
            {
                case YES:
                    OperationNumber();
                    break;
                case NO:
                    Result = 0;
                    OperationNumber();
                    break;
                default:
                    OperationAnswerZero();
                    break;
            }
        }

        public void OperationAnswerExit()
        {
            Answer = WantExitProgram();
            ExitProgram(Answer);
        }

        public void OperationAnswerHistory()
        {
            Answer = WantSeeHistory();
            SeeHistory(Answer);
        }

        public string WantExitProgram()
        {
            Console.WriteLine("Do you want exit the program? yes/no");
            string answer = Console.ReadLine();
            return answer;
        }

        public void ExitProgram(string answer)
        {
            switch (answer)
            {
                case YES:
                    Environment.Exit(0);
                    break;
                case NO:
                    OperationAnswerZero();
                    break;
                default:
                    OperationAnswerExit();
                    break;
            }
        }

        public void PrintHistory()
        {
            foreach (var element in history)
            {
                Console.Write(element);
            }
        }

        public string WantSeeHistory()
        {
            Console.WriteLine("Do you want see the history?yes/no");
            string answer = Console.ReadLine();
            return answer;
        }

        public void SeeHistory(string answer)
        {
            switch (answer)
            {
                case YES:
                    PrintHistory();
                    break;
                case NO:
                    OperationAnswerExit();
                    break;
                default:
                    OperationAnswerHistory();
                    break;
            }
        }
    }
}
