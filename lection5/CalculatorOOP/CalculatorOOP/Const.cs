﻿
namespace CalculatorOOP
{
    abstract class Const
    {
        public readonly string[] ValidInputOperation = new string[] { "1", "2", "3", "4" };
        public readonly string[] ValidAnswer = new string[] { "no", "yes" };
        public const string ONE = "1";
        public const string TWO = "2";
        public const string THREE = "3";
        public const string FOUR = "4";
        public const string YES = "yes";
        public const string NO = "no";
        public const string MULTIPLY = "*";
        public const string DIVIDE = "/";
        public const string ADD = "+";
        public const string SUBSTRACT = "-";
        public const string EQUAIS = "=";
        public const string ENTER_NUMBER = "Exception : Enter the number";
    }
}
