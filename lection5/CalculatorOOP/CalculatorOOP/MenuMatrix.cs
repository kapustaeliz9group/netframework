﻿using System;

namespace CalculatorOOP
{
    class MenuMatrix : Const
    {
        private MatrixBuilder matrix;
        private OperationMatrix operationMatrix;
        private Validation validation;


        public MenuMatrix()
        {
            operationMatrix = new OperationMatrix();
            matrix = new MatrixBuilder();
            validation = new Validation();
        }
       
        public void CalculateMatrix()
        {
            var firstMatrix = matrix.GetMatrixFromConsole("first matrix");
            var secondMatrix = matrix.GetMatrixFromConsole("second matrix");

            Console.WriteLine("The first matrix:");
            matrix.PrintMatrix(firstMatrix);

            Console.WriteLine("The second matrix:");
            matrix.PrintMatrix(secondMatrix);

            Console.WriteLine("Matrix Product:");
            operationMatrix.MatrixMultiplication(firstMatrix, secondMatrix);
            OperationAnswerExitMatrix();
        }

        public void RepeatMatrix()
        {
            while (true)
            {
                CalculateMatrix();
            }
        }

        public void OperationAnswerExitMatrix()
        {
            string answer = null;
            while (true)
            {
                answer = WantExitProgram();
                if (validation.ValidOperation(answer, ValidAnswer))
                {
                    ExitProgramMatrix(answer);
                }
            }
        }
        public void ExitProgramMatrix(string answer)
        {
            switch (answer)
            {
                case YES:
                    Environment.Exit(0);
                    break;
                case NO:
                    RepeatMatrix();
                    break;
                default:
                    RepeatMatrix();
                    break;

            }
        }

        public string WantExitProgram()
        {
            Console.WriteLine("Do you want exit the program? yes/no");
            string answer = Console.ReadLine();
            return answer;
        }
    }
}
