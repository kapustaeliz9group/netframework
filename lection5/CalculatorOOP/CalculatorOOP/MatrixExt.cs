﻿
namespace CalculatorOOP
{
    static class MatrixExt
    {
        public static int RowsCount(this int[,] matrix) => matrix.GetUpperBound(0) + 1;
        public static int ColumnsCount(this int[,] matrix) => matrix.GetUpperBound(1) + 1;
    }

}
