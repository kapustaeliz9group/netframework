﻿using System;

namespace CalculatorOOP
{
    class OperationMatrix
    {
        private MatrixBuilder matrix;
        public OperationMatrix()
        {
            matrix = new MatrixBuilder();
        }

        public void MatrixMultiplication(int[,] matrixA, int[,] matrixB)
        {
            matrix = new MatrixBuilder();
            if (matrixA.ColumnsCount() == matrixB.RowsCount())
            {
                var matrixC = new int[matrixA.RowsCount(), matrixB.ColumnsCount()];

                for (var i = 0; i < matrixA.RowsCount(); i++)
                {
                    for (var j = 0; j < matrixB.ColumnsCount(); j++)
                    {
                        matrixC[i, j] = 0;

                        for (var k = 0; k < matrixA.ColumnsCount(); k++)
                        {
                            matrixC[i, j] += matrixA[i, k] * matrixB[k, j];
                        }
                    }
                }
                matrix.PrintMatrix(matrixC);
            }
            else
            {
                Console.WriteLine("Multiplication is not possible! The number of columns in the first matrix is" +
                    " not equal to the number of rows in the second matrix.");
            }
        }
    }
}
