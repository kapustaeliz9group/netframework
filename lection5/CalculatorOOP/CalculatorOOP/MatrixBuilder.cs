﻿using System;

namespace CalculatorOOP
{
    class MatrixBuilder
    {
        private Validation validation;
        private int numberRows;
        private int numberColumn;

        public MatrixBuilder()
        {
            validation = new Validation();
        }
        public int[,] GetMatrixFromConsole(string name)
        {
            Console.Write("The number of rows of the {0}: ", name);
            numberRows = validation.CheckNumberInt(numberRows);
            Console.Write("The number of column of the {0}: ", name);
            numberColumn = validation.CheckNumberInt(numberColumn); 

            int[,] matrix = new int[numberRows, numberColumn];
            for (int i = 0; i < numberRows; i++)
            {
                for (int j = 0; j < numberColumn; j++)
                {
                    Console.Write("{0}[{1},{2}] = ", name, i, j);
                    while (!int.TryParse(Console.ReadLine(), out matrix[i, j]))
                    {
                        Console.WriteLine("Exception : Enter the number");
                    }
                }
            }

            return matrix;
        }

        public void PrintMatrix(int[,] matrix)
        {
            for (int i = 0; i < matrix.RowsCount(); i++)
            {
                for (int j = 0; j < matrix.ColumnsCount(); j++)
                {
                    Console.Write(matrix[i, j] + "  ");
                }

                Console.WriteLine();
            }
        }

    }
}
