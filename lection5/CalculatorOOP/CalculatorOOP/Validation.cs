﻿using System;
using System.Linq;

namespace CalculatorOOP
{
    class Validation : Const
    {
        public bool ValidOperation(string operation, string[] valid)
        {
            var isValid = valid.Any(value => value.Equals(operation));
            return isValid;
        }

        public double CheckNumberDouble(double number)
        {
            while (!double.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine(ENTER_NUMBER);
            }
            return number;
        }

        public int CheckNumberInt(int number)
        {
            while (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine(ENTER_NUMBER);
            }
            return number;
        }
    }
}
