﻿using System;

namespace CalculatorOOP
{
    class Menu : Const
    {
        private MenuNumbers menuNumbers;
        private MenuMatrix menuMatrix;
        string OperationInput { get; set; }

        public Menu()
        {
            menuNumbers = new MenuNumbers();
            menuMatrix = new MenuMatrix();
        }

        private string GetMenu()
        {
            Console.WriteLine("Choose the operation: \n1. Calculate numbers\n2. Multiply matrix");
            string operation = Console.ReadLine();
            return operation;
        }

        public void Operation()
        {
            OperationInput = GetMenu();
            ChooseOperation(OperationInput);
        }

        public void ChooseOperation(string operation)
        {
            switch (operation)
            {
                case ONE:
                    menuNumbers.OperationNumber();
                    break;
                case TWO:
                    menuMatrix.RepeatMatrix();
                    break;
                default:
                    Operation();
                    break;
            }
        }
    }
}
