﻿using System;

namespace CalculatorOOP
{
    class CalculatorOperation
    {         
        public double Add(double firstNumber, double secondNumber) => firstNumber + secondNumber;

        public double Substract(double firstNumber, double secondNumber) => firstNumber - secondNumber;

        public double Multiply(double firstNumber, double secondNumber) => firstNumber * secondNumber;

        public double Divide(double firstNumber, double secondNumber)
        {
            if (secondNumber == 0)
            {
                throw new ArithmeticException("Can not be divided by zero");
            }
            return firstNumber / secondNumber;
        }      
    }
}
