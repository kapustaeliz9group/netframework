﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace TestProject
{
    [TestClass]
    public class UnitTest1
    {
        private MainPage mainPage;
        private IWebDriver Driver { get; set; }
        private const string VIDEO_NAME = "Massive volcanoes & Flamingo colony - Wild South America - BBC";
        private const string LOGO = "BY";
        private const string ERROR_VIDEO_NAME = "This is not video name";
        private const string ERROR_VIDEO_ID = "This is not video id";

        [TestInitialize]
        public void SetupTest()
        {
            Driver = new ChromeDriver();
            Driver.Manage().Window.Maximize();
            mainPage = new MainPage(Driver);
        }

        [TestCleanup]
        public void TeardownTest()
        {
            Driver.Quit();
        }

        [TestMethod]
        public void CheckVideoTest()
        {
            string videoName = mainPage.GetVideoName();
            Assert.AreEqual(VIDEO_NAME, videoName, ERROR_VIDEO_NAME);
            Assert.AreEqual(LOGO, mainPage.GetId(), ERROR_VIDEO_ID);
        }
    }
}