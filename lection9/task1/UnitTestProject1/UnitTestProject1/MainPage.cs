﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace TestProject
{
    class MainPage
    {
        private IWebElement frame;
        private IWebDriver driver;
        private const string BASE_URL = "https://html.com/tags/iframe/";
        private IWebDriver frameReturn;
        private IWebElement videoElement;
        private const string LINK = "href";
        private const string COUNTRY_CODE = "country-code";
        private const string XPATH_FRAME_HIDDEN = "//iframe[@class='lazy-hidden']";
        private const string XPATH_FRAME = "//iframe[@data-lazy-type='iframe']";
        private const string XPATH_VIDEO_NAME = "//a[@class='ytp-title-link yt-uix-sessionlink']";

        public MainPage(IWebDriver driver)
        {
            this.driver = driver;
            GoToPage();
        }

        public void GoToPage()
        {
            driver.Navigate().GoToUrl(BASE_URL);
        }

        private IWebDriver SwitchElement()
        {
            IWebElement hide = driver.FindElement(By.XPath(XPATH_FRAME_HIDDEN));
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView();"
                                                              , hide);
            frame = driver.FindElement(By.XPath(XPATH_FRAME));
            frameReturn = driver.SwitchTo().Frame(frame);
            return frameReturn;
        }

        public string GetLinkVideo()
        {
            new WebDriverWait(frameReturn, TimeSpan.FromSeconds(30))
                .Until(ExpectedConditions
                .ElementIsVisible(By.XPath(XPATH_VIDEO_NAME)));
            videoElement = frameReturn.FindElement(By.XPath(XPATH_VIDEO_NAME));
            return videoElement.GetAttribute(LINK);
        }

        public string GetId()
       {
            driver.Navigate().GoToUrl(GetLinkVideo());
            new WebDriverWait(frameReturn, TimeSpan.FromSeconds(30))
                .Until(ExpectedConditions
                .ElementIsVisible(By.Id(COUNTRY_CODE)));
            return  driver.FindElement(By.Id(COUNTRY_CODE)).Text;
        }

        public string GetVideoName()
        {
            new WebDriverWait(SwitchElement(), TimeSpan.FromSeconds(30))
                .Until(ExpectedConditions
                .ElementIsVisible(By.XPath(XPATH_VIDEO_NAME)));
            videoElement = frameReturn.FindElement(By.XPath(XPATH_VIDEO_NAME));
            return videoElement.Text;
        }
    }
}
