using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TestProjectGitHub.Pages;

namespace TestProjectGitHub
{
    [TestClass]
    public class Test
    {
        private IWebDriver Driver { get; set; }
        private FirstRepoPage firstRepoPage;
        private SeleniumDotNetPage seleniumDotNetPage;
        private const string CHECK_TEXT = "IHasInputDevices Interface";
        private const string ERROR_PAGE = "This is not same page";
        private const string ERROR_TEXT = "Element does not contains this text ";

        [TestInitialize]
        public void SetupTest()
        {
            Driver = new ChromeDriver();
            Driver.Manage().Window.Maximize();
        }

        [TestCleanup]
        public void TearDown()
        {
            Driver.Quit();
        }

        [TestMethod]
        public void TaskTest()
        {
            firstRepoPage = new FirstRepoPage(Driver);
            Assert.AreEqual(firstRepoPage.CheckRepoBeforeRefresh(),
                firstRepoPage.CheckRepoAfterRefresh(), ERROR_PAGE);
            firstRepoPage.Open();
            seleniumDotNetPage = new SeleniumDotNetPage(Driver);
            seleniumDotNetPage.WriteHtmlFile();
            Assert.IsTrue(seleniumDotNetPage.htmlCode
                .Contains(CHECK_TEXT), ERROR_TEXT);
        }
    }
}
