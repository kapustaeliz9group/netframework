﻿
using OpenQA.Selenium;

namespace TestProjectGitHub.Pages
{
    class ActionPage
    {
        private IWebDriver driver;
        private const string XPATH_INPUT_SEARCH = "//input[@placeholder='Search GitHub']";
        private const string TEXT_INPUT_SEARCH = "Selenium";

        public ActionPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public ResultSearchPage Search()
        {
            MainPage mainPage = new MainPage(driver);
            mainPage.ChooseAction();
            var searchInput = driver.FindElement(By.XPath(XPATH_INPUT_SEARCH));
            searchInput.SendKeys(TEXT_INPUT_SEARCH);
            searchInput.SendKeys(Keys.Enter);
            return new ResultSearchPage(driver);
        }
    }
}
