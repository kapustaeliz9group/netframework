﻿using OpenQA.Selenium;
using System.IO;
using System.Net;
using System.Text;

namespace TestProjectGitHub.Pages
{
    public class SeleniumDotNetPage
    {
        private IWebDriver driver;
        private const string BASE_URL = "https://selenium.dev/selenium/docs/api/dotnet/";
        public string htmlCode;

        public SeleniumDotNetPage(IWebDriver driver)
        {
            this.driver = driver;
            GoToPage();
        }

        public void GoToPage()
        {
            driver.Navigate().GoToUrl(BASE_URL);
        }

        public void WriteHtmlFile()
        {
            htmlCode = GetCode();
            TextWriter tw = new StreamWriter("html.txt");
            tw.WriteLine(htmlCode);
            tw.Close();
        }

        public string GetCode()
        {
            string data = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(BASE_URL);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = null;
                if (response.CharacterSet == null)
                {
                    readStream = new StreamReader(receiveStream);
                }
                else
                {
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                }
                data = readStream.ReadToEnd();
                response.Close();
                readStream.Close();
            }
            return data;
        }
    }
}
