﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;

namespace TestProjectGitHub.Pages
{
    class MainPage
    {
        private IWebDriver driver;
        private const string BASE_URL = "https://github.com/";
        private const string XPATH_BUTTON_ACTIONS = "//ul[@class ='list-style-none f5 pb-3']//li//a[contains(text(),'Actions')]";
        private const string XPATH_BUTTON_WHY_GITHUB = "//summary[contains(text(),'Why GitHub?')]";

        public MainPage(IWebDriver driver)
        {
            this.driver = driver;
            GoToPage();
        }

        public void GoToPage()
        {
            driver.Navigate().GoToUrl(BASE_URL);
        }

        public void OpenDropDown()
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(30))
                .Until(ExpectedConditions.ElementToBeClickable(By.XPath(XPATH_BUTTON_WHY_GITHUB)));
            new Actions(driver).MoveToElement(driver.FindElement(By
                .XPath(XPATH_BUTTON_WHY_GITHUB))).Build().Perform();
            try
            {
                driver.FindElement(By.XPath(XPATH_BUTTON_ACTIONS)).Click();
            }
            catch
            {
                OpenDropDown();
            }
        }

        public ActionPage ChooseAction()
        {
            OpenDropDown();
            return new ActionPage(driver);
        }
    }
}
