﻿using OpenQA.Selenium;


namespace TestProjectGitHub.Pages
{
    class ResultSearchPage
    {
        private ActionPage actionPage;
        private IWebDriver driver;
        private const string XPATH_FIRST_REPO = "//div[@class='col-12 col-md-9 float-left px-2 pt-3 pt-md-0 codesearch-results']//li[1]";
        private const string XPATH_LINK_FIRST_REPO = "//li[1]//div[1]//h3[1]//a[1]";

        public ResultSearchPage(IWebDriver driver)
        {
            this.driver = driver;            
        }

        public FirstRepoPage OpenFirstRepo()
        {
            actionPage = new ActionPage(driver);
            actionPage.Search();
            driver.FindElement(By.XPath(XPATH_FIRST_REPO));
            var nameFirstRepo = driver.FindElement(By.XPath(XPATH_LINK_FIRST_REPO));
            nameFirstRepo.Click();
            return new FirstRepoPage(driver);
        }
    }
}
