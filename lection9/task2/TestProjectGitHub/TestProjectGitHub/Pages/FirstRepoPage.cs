﻿using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace TestProjectGitHub.Pages
{
    class FirstRepoPage
    {
        private ResultSearchPage resultSearchPage;
        private IWebDriver driver;
        private IWebElement checkRepo;
        public string linkRepoBeforeRefresh;
        public string linkRepoAfterRefresh;
        private const string LINK = "href";
        private const string LINK_FIRST_REPO = "//h1[contains(@class,'public')]//strong//a";

        public FirstRepoPage(IWebDriver driver)
        {
            this.driver = driver;
            resultSearchPage = new ResultSearchPage(driver);
        }

        public void CheckRepo()
        {
            resultSearchPage.OpenFirstRepo();
            checkRepo = driver.FindElement(By.XPath(LINK_FIRST_REPO));
        }

        public string CheckRepoBeforeRefresh()
        {
            CheckRepo();
            return checkRepo.GetAttribute(LINK);
        }

        public string CheckRepoAfterRefresh()
        {
            CheckRepo();
            driver.Navigate().Refresh();
            checkRepo = driver.FindElement(By.XPath(LINK_FIRST_REPO));
            return checkRepo.GetAttribute(LINK);
        }

        public void Open()
        {
            IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
            jse.ExecuteScript("window.open()");
            driver.Close();
            driver.SwitchTo().Window(driver.WindowHandles.Last());          
        }
    }
}
