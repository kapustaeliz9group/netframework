﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;

namespace SeleniumFramework.Pages
{
    public class ContactPage
    {
        private IWebDriver driver;

        [FindsBy(How = How.XPath, Using = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/label[1]/input[1]")]
        public IWebElement Email { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='goog_749628404']")]
        public IWebElement BookingNumber { get; set; }

        public ContactPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
            new WebDriverWait(driver, TimeSpan.FromSeconds(30));
        }

        public void AskQuestion(string email, string bookingNumber)
        {
            HomePage home = new HomePage(driver);
            home.goToPage();
            home.CustomerSupport();
            //Email.SendKeys(email);
            //BookingNumber.SendKeys(bookingNumber);
        }

        

    }
}
