﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using SeleniumFramework.Model;
using System;

namespace SeleniumFramework.Pages
{
    public class HomePage
    {
        public const string XPATHEMAIL = "input.zenforminput-control[name='email']";
        public const string XPATHPASSWORD = "input.zenforminput-control[name='pass']";
        public const string XPATHNAMECITY = "//*[@id=':0']/div/div/div[1]/div/div[3]/div/div/div/div[1]/div/label/div[2]/input";
        public const string XPATHNUMBER = "//div[@class='Day__inner--1kyRv Day__inner_locked--2etRy Day__inner_selected--3eqFt Day__inner_edgeDay--1K1eT Day__inner_withLeftRadius--3nDHR']";
        private IWebDriver driver;
        IWebElement source;

        [FindsBy(How = How.XPath, Using = "/html/body/div[1]/div[2]/div/div/div[4]")]
        public IWebElement PersonalAccount { get; set; }

        [FindsBy(How = How.CssSelector, Using = XPATHEMAIL)]
        public IWebElement PersonalAccountEmail { get; set; }

        [FindsBy(How = How.CssSelector, Using = XPATHPASSWORD)]
        public IWebElement PersonalAccountPassword { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div[1]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div/div/form/div[2]/button[1]")]
        public IWebElement ButtonLogin { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='SearchForm__dates--3XSQc SearchForm__control--2yett']//label[1]")]
        public IWebElement ArrivalDate { get; set; }

        [FindsBy(How = How.XPath, Using = XPATHNUMBER)]
        public IWebElement TitleArrivalDate { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=':0']/div/div/div[1]/div/div[3]/div/div/div/div[3]/div/label/div[2]/div[2]")]
        public IWebElement Guests { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='Room__line--385ZF']//div[1]//div[2]//button[1]")]
        public IWebElement ButtonPlusGuests { get; set; }

        [FindsBy(How = How.XPath, Using = XPATHNAMECITY)]
        public IWebElement Distantion { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=':0']/div/div/div[1]/div/div[1]")]
        public IWebElement TitleDistantion { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=':0']/div/div/div[1]/div/div[3]/div/div/div/div[4]/button")]
        public IWebElement ButtonSearch { get; set; }

        [FindsBy(How = How.XPath, Using = "//select[@class='zen-header-select-list']")]
        public IWebElement ButtonChangeLanguage { get; set; }

        [FindsBy(How = How.XPath, Using = "//option[contains(text(),'Español')]")]
        public IWebElement EnglishLanguage { get; set; }

        [FindsBy(How = How.XPath, Using = "//option[contains(text(),'USD')]")]
        public IWebElement ValueUSD { get; set; }

        [FindsBy(How = How.XPath, Using = "//select[@class='zen-currency-select-list']")]
        public IWebElement ButtonChangeValue { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='zenheaderhelpcenter-button']")]
        public IWebElement ButtonOnlineChat { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='zenheaderhelpcenter-menu-send-message-chat-button button-view-primary button-size-m']")]
        public IWebElement ChatWebSite { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='scroll-content']//input[@name='name']")]
        public IWebElement ChatName { get; set; }

        [FindsBy(How = How.XPath, Using = "//body[@class='ru mobileready body-home body-home-loaded']/div[@id='webim_chat']/div[@class='webim-chat-block']/div[@class='webim-chat']/div[@class='webim-resizable ui-resizable']/div[@class='webim-body']/div[@class='webim-sections']/div[@class='webim-section webim-section-online']/div[@class='webim-section webim-section-first-question']/div[@class='webim-question-block webim-ready']/div[@class='webim-auto-height-wrapper']/div[@class='scroll-wrapper']/div[@class='scroll-content scroll-scrolly_visible']/div[@class='webim-form']/div[@class='webim-form-control']/label[@class='webim-label webim-js-overlabel']/span[1] ")]
        public IWebElement ChatMessage { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='zen-headermenu-wrapper']")]
        public IWebElement MenuButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='layout layout-desktop']//li[4]")]
        public IWebElement CustomerButton { get; set; }

        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
            new WebDriverWait(driver, TimeSpan.FromSeconds(30));
        }

        public void goToPage()
        {
            driver.Navigate().GoToUrl("https://ostrovok.ru/");
        }

        public void SignIn(User user)
        {
            this.PersonalAccount.Click();
            this.PersonalAccountEmail.Click();
            this.PersonalAccountEmail.SendKeys(user.Email);
            this.PersonalAccountPassword.Click();
            this.PersonalAccountPassword.SendKeys(user.Password);
            this.ButtonLogin.Click();
        }

        public void PutMessageChat(string name, string message)
        {
            ButtonOnlineChat.Click();
            ChatWebSite.Click();
            driver.SwitchTo().Window("webim-chat");
            ChatName.SendKeys(name);
            ChatMessage.SendKeys(message);
        }

        public void ChangeLanguage()
        {
            ButtonChangeLanguage.Click();
            EnglishLanguage.Click();
        }

        public void ChangeValue()
        {
            ButtonChangeValue.Click();
            ValueUSD.Click();
        }

        public SearchResultPage Search(string city)
        {
             //this.ArrivalDate.Click();
            //this.TitleArrivalDate.Click();
             this.Guests.Click();
             this.ButtonPlusGuests.Click();
             this.Distantion.Click();
             this.Distantion.Clear();
             this.Distantion.SendKeys(city);
             this.TitleDistantion.Click();
             this.ButtonSearch.Click();
            return new SearchResultPage(driver);
         }

        public ContactPage CustomerSupport()
        {
            MenuButton.Click();
            CustomerButton.Click();
            return new ContactPage(driver);
        }
}
}
