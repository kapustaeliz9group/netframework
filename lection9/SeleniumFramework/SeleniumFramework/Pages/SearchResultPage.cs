﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;

namespace SeleniumFramework.Pages
{
    public class SearchResultPage
    {
        [FindsBy(How = How.XPath, Using = "//div[@class='zenformselect zenformselect-size-xs']")]
        public IWebElement Sorting { get; set; }

        [FindsBy(How = How.XPath, Using = "//li[@class='zendropdownlist-item zendropdownlist-item-price.desc']")]
        public IWebElement FirstExpensiveHotels { get; set; }
        private IWebDriver driver;

        public SearchResultPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
            new WebDriverWait(driver, TimeSpan.FromSeconds(30));
        }

        public void SortingHotels(string city)
        {
            HomePage homePage = new HomePage(driver);
            homePage.goToPage();
            homePage.Search(city);
            Sorting.Click();
            FirstExpensiveHotels.Click();
        }


    }
}
