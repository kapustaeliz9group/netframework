﻿using OpenQA.Selenium;
using SeleniumFramework.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumFramework.Steps
{
    public class Step
    {
        public IWebDriver driver;
        public IWebElement source;

        public void InitBrowser()
        {
            driver = Driver.DriverInstance.GetInstance();
        }

        public void CloseBrowser()
        {
            Driver.DriverInstance.CloseBrowser();
        }

         

        public IWebElement FindElementByLocator(By locator)
        {
            try
            {
                source = driver.FindElement(locator);
                return source;
            }
            catch
            {
                return null;
            }
        }

        
}
}
