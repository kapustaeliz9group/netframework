﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using SeleniumFramework.Model;
using SeleniumFramework.Pages;
using SeleniumFramework.Steps;
using System;

namespace Test
{
    [TestClass]
    public class UnitTest1
    {
        public const string CITY = "Москва";
        private const string LANGUAGE = "Русский";
        public const string BUTTON_CITY = "Москва, Россия";
        private const string ERROR_MESSAGE = "Указан неправильный пароль или электронный адрес";
        private const string CHAT_MESSAGE = "Не могли бы проконсультировать по отелю";
        private const string USER_NAME = "Дмитрий";
        private const string VALUE = "BYN";
        private const string XPATH_LANGUAGE = "//div[@class='zen-header-select-value']";
        private const string XPATH_VALUE = "//div[@class='zen-currency-select-value']";
        private const string XPATH_ERROR_MESSAGE = "zen-authpane-signin-error-message";
        public const string XPATHNAMECITY = "//*[@id=':0']/div/div/div[1]/div/div[3]/div/div/div/div[1]/div/label/div[2]/input";
        public IWebDriver Driver { get; set; }
        Step step = new Step();

        [TestInitialize]
        public void SetupTest()
        {
            step.InitBrowser();
        }

        [TestCleanup]
        public void TeardownTest()
        {
           //step.CloseBrowser();
        }

        [TestMethod]
        public void CustomerSupportTest()
        {
            HomePage main = new HomePage(step.driver);
            main.goToPage();
            main.CustomerSupport();
            Assert.AreEqual("Contact our Customer Service quickly", step.FindElementByLocator(By.XPath("//h1[@class='zenfeedback-form-title']")).Text);
        }

        [TestMethod]
        public void SignUpErrorTest()
        {
            HomePage homePage = new HomePage(step.driver);
            User user = new User("example@gmail.com", "example12345");
            homePage.goToPage();
            homePage.SignIn(user);
            Assert.AreEqual(ERROR_MESSAGE, step.FindElementByLocator(By.ClassName(XPATH_ERROR_MESSAGE)).Text);
        }

        [TestMethod]
        public void ChangeLanguageTest()
        {
            HomePage homePage = new HomePage(step.driver);
            homePage.goToPage();
            //Assert.AreEqual(LANGUAGE, step.FindElementByLocator(By.XPath(XPATH_LANGUAGE)).Text);
            homePage.ChangeLanguage();
        }

        [TestMethod]
        public void ChangeValueTest()
        {
            HomePage homePage = new HomePage(step.driver);
            homePage.goToPage();
            Assert.AreEqual(VALUE, step.FindElementByLocator(By.XPath(XPATH_VALUE)).Text);
            homePage.ChangeValue();
        }

        [TestMethod]
        public void SearchTest()
        {
            HomePage homePage = new HomePage(step.driver);
            homePage.goToPage(); 
            homePage.Search(CITY);
            Assert.AreEqual("Moscow, Russia", step.FindElementByLocator(By.XPath("//div[@class='zenbreadcrumbs-item-row'][contains(text(),'in Moscow')]")).Text);
        }

        [TestMethod]
        public void PutMessageChatTest()
        {
            HomePage homePage = new HomePage(step.driver);
            homePage.goToPage();
            homePage.PutMessageChat(USER_NAME,CHAT_MESSAGE);
        }

        [TestMethod]
        public void SortingHotelsTest()
        {
            SearchResultPage searchResultPage = new SearchResultPage(step.driver);
           
           // searchResultPage.SortingHotels(CITY);
        }
    }
}
