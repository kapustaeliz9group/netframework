﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using SeleniumFramework.Pages;
using SeleniumFramework.Steps;

namespace Test
{
    [TestClass]
    public class ContactPageTest
    {
        Step step = new Step();

        [TestInitialize]
        public void SetupTest()
        {
            step.InitBrowser();
        }

        [TestCleanup]
        public void TeardownTest()
        {
            //step.CloseBrowser();
        }

        [TestMethod]
        public void AskQuestionTest()
        {
            ContactPage contactPage = new ContactPage(step.driver);
            contactPage.AskQuestion("example@mail.ru", "3");
            Assert.AreEqual("Contact our Customer Service quickly", step.FindElementByLocator(By.XPath("//p[contains(text(),'Email address')]")).Text);
        }

    }
}
