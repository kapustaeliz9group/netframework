﻿using NUnit.Framework;
using Project.Pages.Alerts;
using System;
using TestProject.Tests;

namespace Project.Tests.AlertsTest
{
    [TestFixture]
    public class AlertPageTest : BasePageTest
    {
        public const string TEXT_POMP_UP = "Do you like toolsqa?";
        public const string COMENT_POMP_UP = "Great site";

        [Test]
        public void AlertTest()
        {
            HandlingAlertsPage handlingAlertsPage = new HandlingAlertsPage(Driver);
            Console.WriteLine(handlingAlertsPage.ChooseSimpleAlert());
            Console.WriteLine(handlingAlertsPage.AcceptConfirmPopUp());
            handlingAlertsPage.DismissConfirmPopUp();
            Console.WriteLine(handlingAlertsPage.ChoosePomptPopUp(COMENT_POMP_UP));
            Assert.AreEqual(TEXT_POMP_UP, handlingAlertsPage.ChoosePomptPopUp(COMENT_POMP_UP), "Text aren't equal");
        }
    }
}
