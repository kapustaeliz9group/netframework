﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace TestProject.Tests
{
    [TestFixture]
    public class BasePageTest
    {
        public IWebDriver Driver { get; set; }

        [SetUp]
        public void SetupTest()
        {
            Driver = new ChromeDriver();
            Driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void TearDown()
        {
            Driver.Quit();
        }
    }
}
