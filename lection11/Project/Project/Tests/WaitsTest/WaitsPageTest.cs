﻿using NUnit.Framework;
using OpenQA.Selenium;
using Project.Pages.Waits;
using TestProject.Tests;

namespace Project.Tests.WaitsTest
{
    [TestFixture]
    public class WaitsPageTest : BasePageTest
    {
        public const string COMPARE_TITLE = "Сравнение товаров";

        [Test]
        public void WaitsTest()
        {
            CompareResultPage compareResultPage = new CompareResultPage(Driver);
            compareResultPage.CompareTV();
            Assert.AreEqual(COMPARE_TITLE, Driver.FindElement(By.XPath(CompareResultPage.XPATH_COMPARE_TITLE)).Text, "Text aren't equal");
        }
    }
}
