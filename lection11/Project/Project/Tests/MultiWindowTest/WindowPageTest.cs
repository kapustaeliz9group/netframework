﻿using NUnit.Framework;
using Project.MultiWindow.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using TestProject.Tests;

namespace Project.Tests.MultiWindowTest
{
    [TestFixture]
    public class WindowPageTest : BasePageTest
    {
        private const int COUNT_WINDOWS = 3;
        private const string CATALOG_ONLINER = "Каталог Onliner";
        private const string TITLE_GOOGLE_PLAY_ENGLISH_PAGE = "Каталог Onliner - Apps on Google Play";
        private const string TITLE_GOOGLE_PLAY_RUSSIAN_PAGE = "Приложения в Google Play – Каталог Onliner";
        private const string TITLE_APP_STORE_PAGE = "Каталог Onliner on the App Store";
        private const string TITLE_SEARCH_TV_PAGE = "Телевизор купить в Минске";

        [Test]
        public void WindowTest()
        {
            SearchTVPage searchTVPage = new SearchTVPage(Driver);
            AppStoreOnlinerPage appStoreOnlinerPage = searchTVPage.OpenAppStore();
            GooglePlayOnlinerPage googlePlayOnlinerPage = searchTVPage.OpenGooglePlay();
            Assert.AreEqual(COUNT_WINDOWS, Driver.WindowHandles.Count, "Open windows aren't three");
            searchTVPage.SwitchWindows(TITLE_GOOGLE_PLAY_RUSSIAN_PAGE);
            Assert.AreEqual(CATALOG_ONLINER, googlePlayOnlinerPage.CatalogOnliner.Text, "This isn't catalog onliner");
            GooglePlayOnlinerPage sameApplicationPage = googlePlayOnlinerPage.OpenSameApplications();
            Console.WriteLine("Number of same application: " + sameApplicationPage.GetCountSameApplication());
            searchTVPage.SwitchWindows(TITLE_APP_STORE_PAGE);
            appStoreOnlinerPage.OpenMoreInformation();
            Driver.Close();
            searchTVPage.SwitchWindows(TITLE_SEARCH_TV_PAGE);
            searchTVPage.Advertising.Click();
            Assert.AreEqual(COUNT_WINDOWS, Driver.WindowHandles.Count, "Advertising page haven't opened");
        }
    }
}
