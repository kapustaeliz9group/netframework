﻿using NUnit.Framework;
using Project.Pages.Frames;
using TestProject.Tests;

namespace Project.Tests.FramesTest
{
    [TestFixture]
    public class FramesPageTest : BasePageTest
    {
        private const string CODE = "<input id=\"test\"/>";
        private const string TEXT = "Hello";

        [Test]
        public void FrameTest()
        {
            FramesPage framesPage = new FramesPage(Driver);
            framesPage.InputText(CODE, TEXT);
            Assert.AreEqual(TEXT, framesPage.Input.GetAttribute("value"), "Input text aren't equal");
        }
    }
}
