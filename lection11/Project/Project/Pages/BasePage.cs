﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Linq;

namespace TestProject.Pages
{
    public class BasePage
    {
        protected IWebDriver driver;

        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public void GoToPage(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public void SwitchWindows(string title)
        {
            List<string> allWindowsHandles = driver.WindowHandles.ToList();
            foreach (string handle in allWindowsHandles)
            {
                string switchedWindowTitle = driver.SwitchTo().Window(handle).Title;
                if (title.Equals(switchedWindowTitle))
                {
                     break;
                }
            }
        }
    }
}
