﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using TestProject.Pages;

namespace Project.MultiWindow.Pages
{
    public class AppStoreOnlinerPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//div/button[text()='more']")]
        public IWebElement ButtonMore { get; set; }
        public AppStoreOnlinerPage(IWebDriver _driver) : base(_driver)
        {
        }

        public void OpenMoreInformation()
        {
            ButtonMore.Click();
        }
    }
}
