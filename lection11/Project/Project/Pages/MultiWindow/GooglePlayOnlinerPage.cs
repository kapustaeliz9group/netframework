﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Linq;
using System.Threading;
using TestProject.Pages;

namespace Project.MultiWindow.Pages
{
    public class GooglePlayOnlinerPage : BasePage
    {
        private const string XPATH_YET = "//div[@class ='ImZGtf mpg5gc']";

        [FindsBy(How = How.XPath, Using = "//a[contains(@class,'id-track-click')]")]
        public IWebElement ButtonYet { get; set; }

        [FindsBy(How = How.XPath, Using = "//h1[@class='AHFaub']//span[contains(text(),'Onliner')]")]
        public IWebElement CatalogOnliner { get; set; }

        public GooglePlayOnlinerPage(IWebDriver _driver) : base(_driver){}

        public GooglePlayOnlinerPage OpenSameApplications()
        {
            ButtonYet.Click();
            return new GooglePlayOnlinerPage(driver);
        }

        public int GetCountSameApplication()
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(70)).Until(ExpectedConditions.ElementExists(By.XPath(XPATH_YET)));
            return driver.FindElements(By.XPath(XPATH_YET)).Count;
        }
    }
}
