﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using TestProject.Pages;

namespace Project.MultiWindow.Pages
{
    public class SearchTVPage : BasePage
    {
        private const string BASE_URL = "https://catalog.onliner.by/tv";
        public int countWindow;
        public static List<string> allWindowsHandles;

        [FindsBy(How = How.XPath, Using = "//a[@class='schema-filter__store-item schema-filter__store-item_apple']")]
        public IWebElement ButtonAppStore { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@class='schema-filter__store-item schema-filter__store-item_google']")]
        public IWebElement ButtonGooglePlay { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@id='adfox_15764978254342199']")]
        public IWebElement Advertising { get; set; }

        public SearchTVPage(IWebDriver _driver) : base(_driver) 
        {
            GoToPage(BASE_URL);
        }


        private void Scroll()
        {
            IWebElement hide = driver.FindElement(By.XPath("//div[@class='schema-filter__store-list']"));
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView();"
                                                              , hide);
        }

        public AppStoreOnlinerPage OpenAppStore()
        {
            Scroll();
            ButtonAppStore.Click();
            return new AppStoreOnlinerPage(driver);
        }

        public GooglePlayOnlinerPage OpenGooglePlay()
        {
            Scroll();
            ButtonGooglePlay.Click();
            return new GooglePlayOnlinerPage(driver);
        }
    }
}
