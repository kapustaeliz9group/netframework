﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using TestProject.Pages;

namespace Project.Pages.Frames
{
    public class FramesPage : BasePage
    {
        private const string BASE_URL = "http://jsbin.com/?html,output";

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'CodeMirror-activeline')]//pre[contains(@class,'CodeMirror-line')]/span")]
        public IWebElement InputTextCode { get; set; }

        [FindsBy(How = How.Name, Using = "JS Bin Output ")]
        public IWebElement FrameOutput { get; set; }

        [FindsBy(How = How.XPath, Using = "//iframe[@class='stretch']")]
        public IWebElement FrameMain { get; set; }

        [FindsBy(How = How.XPath ,Using = "//html/body/input")]
        public IWebElement Input { get; set; }

        public FramesPage(IWebDriver driver) : base(driver)
        {
            GoToPage(BASE_URL);
        }

        public void InputText(string code, string text)
        {
            new Actions(driver).SendKeys(InputTextCode, code).Build().Perform();
            new WebDriverWait(driver, TimeSpan.FromSeconds(30))
                .Until(ExpectedConditions.ElementExists(By.XPath("//span[@style='display: none;']")));
            driver.SwitchTo().Frame(FrameMain);
            driver.SwitchTo().Frame(FrameOutput);
            Input.SendKeys(text);
        }
    }
}
