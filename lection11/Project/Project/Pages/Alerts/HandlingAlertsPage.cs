﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using TestProject.Pages;

namespace Project.Pages.Alerts
{
    class HandlingAlertsPage : BasePage
    {
        private IAlert alert;
        private const string BASE_URL= "https://www.toolsqa.com/handling-alerts-using-selenium-webdriver/";
        private const string XPATH_PROMPT_UP = "//button[contains(text(),'Prompt Pop up')]";

        [FindsBy(How = How.XPath, Using = "//button[contains(text(),'Simple Alert')]")]
        public IWebElement SimpleAlertButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Different types of Alerts')]")]
        public IWebElement Text { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[contains(text(),'Confirm Pop up')]")]
        public IWebElement ButtonConfirmPopUp { get; set; }

        [FindsBy(How = How.XPath, Using = XPATH_PROMPT_UP)]
        public IWebElement ButtonPromptPopUp { get; set; }

        public HandlingAlertsPage(IWebDriver _driver) : base(_driver)
        {
            GoToPage(BASE_URL);
        }

        public string  ChooseSimpleAlert()
        {
            SwitchToAlert(SimpleAlertButton);
            string alertText =  alert.Text;
            alert.Dismiss();
            return alertText;
        }

        public void SwitchToAlert(IWebElement element)
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView();"
                                                             , Text);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(35));
            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(XPATH_PROMPT_UP)));
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].click();"
                                                             , element);
            try
            {
                wait.Until(ExpectedConditions.AlertIsPresent());
                alert = driver.SwitchTo().Alert();
            }
            catch
            {
                SwitchToAlert(element);
            }
        }

        public string AcceptConfirmPopUp()
        {
            SwitchToAlert(ButtonConfirmPopUp);
            string alertText = alert.Text;
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            alert.Accept();
            return alertText;
        }

        public void DismissConfirmPopUp()
        {
            SwitchToAlert(ButtonConfirmPopUp);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            alert.Dismiss();
        }

        public string ChoosePomptPopUp(string text)
        {
            SwitchToAlert(ButtonPromptPopUp);
            string alertText = alert.Text;
            alert.SendKeys(text);
            alert.Accept();
            return alertText;
        }
    }
}
