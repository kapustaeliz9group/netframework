﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using TestProject.Pages;

namespace Project.Pages.Waits
{
    class SearchTVPage : BasePage
    {
        private const string XPATH_BUTTON_COMPARE = "//div[@class='compare-button__state compare-button__state_initial']";
        private const string BASE_URL = "https://catalog.onliner.by/tv";

        [FindsBy(How = How.XPath, Using = "//div[@class='schema-products']/div[1]/div/div/div[@class='schema-product__compare']")]
        public IWebElement ButtonChooseFirstTV { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='schema-products']/div[2]/div/div/div[@class='schema-product__compare']")]
        public IWebElement ButtonChooseSecondTV { get; set; }

        [FindsBy(How = How.XPath, Using = XPATH_BUTTON_COMPARE)]
        public IWebElement ButtonCompare { get; set; }
        public SearchTVPage(IWebDriver driver) : base(driver)
        {
            GoToPage(BASE_URL);
        }

        public void CompareTwoFirstTV()
        {
            ButtonChooseFirstTV.Click();
            ButtonChooseSecondTV.Click();
            new WebDriverWait(driver, TimeSpan.FromSeconds(30)).Until(ExpectedConditions.ElementToBeClickable(By.XPath(XPATH_BUTTON_COMPARE)));
            ButtonCompare.Click();
        }
    }
}
