﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using TestProject.Pages;

namespace Project.Pages.Waits
{
    public class CompareResultPage : BasePage
    {
        private const string XPATH_ICON_QUESTION = "//span[contains(.,'Диагональ экрана')]/../div/span";
        private const string XPATH_HINT = "//div[@class='product-table-tip__content']";
        public const string XPATH_COMPARE_TITLE = "//h1[@class='b-offers-title']";

        [FindsBy(How = How.XPath, Using = "//span[contains(.,'Диагональ экрана')]/../div/span")]
        public IWebElement IconQuestion { get; set; }

        [FindsBy(How = How.XPath, Using = "//tbody[6]//tr[4]")]
        public IWebElement ScreenDiagonal { get; set; }

        [FindsBy(How = How.XPath, Using = "//tr[@class='product-table__row product-table__row_header product-table__row_top']/th[3]/div[1]/a[1]")]
        public IWebElement DeleteTV { get; set; }
        public CompareResultPage(IWebDriver driver) : base(driver) { }

        public void CompareTV()
        {
            SearchTVPage searchTVPage = new SearchTVPage(driver);
            searchTVPage.CompareTwoFirstTV();
            new Actions(driver).MoveToElement(ScreenDiagonal).Build().Perform();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(XPATH_ICON_QUESTION)));
            IconQuestion.Click();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(XPATH_HINT)));
            ScreenDiagonal.Click();
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath(XPATH_HINT)));
            DeleteTV.Click();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(XPATH_COMPARE_TITLE)));
        }
    }
}
