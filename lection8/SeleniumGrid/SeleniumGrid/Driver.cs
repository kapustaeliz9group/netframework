﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;

namespace SeleniumGrid
{
    class Driver
    {
        public const string CHROME = "Chrome";
        public const string FIREFOX = "Firefox";
        private static readonly string UrlRemoteAddress = "http://localhost:4444/wd/hub";

        public static IWebDriver GetDriver(string browser)
        {
            switch (browser)
            {
                case CHROME:
                    return new RemoteWebDriver(new Uri(UrlRemoteAddress), new ChromeOptions().ToCapabilities());

                default:
                    return new RemoteWebDriver(new Uri(UrlRemoteAddress), new FirefoxOptions().ToCapabilities());
            }
        }
    }
}
