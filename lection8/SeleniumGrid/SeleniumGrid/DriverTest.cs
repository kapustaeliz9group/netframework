using NUnit.Framework;
using OpenQA.Selenium;

namespace SeleniumGrid
{
    public class Tests
    {
        private IWebDriver _driver;


        [Test]
        public void InitializeChrome()
        {
            _driver = Driver.GetDriver(Driver.CHROME);
            _driver.Navigate().GoToUrl("https://github.com");
        }

        [Test]
        public void InitializeFireFox()
        {
            _driver = Driver.GetDriver(Driver.FIREFOX);
            _driver.Navigate().GoToUrl("https://github.com");
        }

        [TearDown]
        public void CloseBrowser()
        {
            _driver.Quit();
        }
    }
}