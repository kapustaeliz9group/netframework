﻿using OpenQA.Selenium.Remote;
using System;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;

namespace ExampleSeleniumGrid
{
    class Program
    {
        public static RemoteWebDriver driver;
        static void Main(string[] args)
        {
            var caps = new RemoteSessionSettings();

            caps.AddMetadataSetting("name", "C Sharp Test");
            caps.AddMetadataSetting("browserName", "Chrome");
            caps.AddMetadataSetting("platform", "Windows 10");

            driver = new RemoteWebDriver(new Uri("http://192.168.59.1:4444/wd/hub"), caps);

            driver.Navigate().GoToUrl("https://github.com/");

        }
    }
}
