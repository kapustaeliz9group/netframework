﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using task2.Pages;

namespace task2.Test 
{
    [TestClass]
    public class DroppablePageTest
    {
        private const string COLOR_DROPPABLE = "rgba(255, 250, 144, 1)";
        private const string CSS_VALUE_BACKGROUND = "background-color";
        private DroppablePage droppablePage;
        private IWebDriver Driver { get; set; }

        [TestInitialize]
        public void SetupTest()
        {
            Driver = new ChromeDriver();
            Driver.Manage().Window.Maximize();
        }

        [TestCleanup]
        public void TearDown()
        {
            Driver.Quit();
        }

        [TestMethod]
        public void DragAndDropTest()
        {
            droppablePage = new DroppablePage(Driver);
            droppablePage.DragAndDrop();
            Assert.AreEqual(COLOR_DROPPABLE, droppablePage
                .DroppableSquere.GetCssValue(CSS_VALUE_BACKGROUND),
                "This color is not yellow");
            Assert.AreEqual("Dropped!",droppablePage.DroppedText.Text,
                "This text is not displayed ");
        }
    }
}
