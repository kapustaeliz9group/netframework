﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;


namespace task2.Pages
{
    class MainPage : BasePage
    {
        private const string BASE_URL = "https://demoqa.com";

        [FindsBy(How = How.LinkText, Using = "Droppable")]
        public IWebElement DroppableButton { get; set; }

        public MainPage(IWebDriver driver) : base(driver)
        {
             GoToPage();
        }

        public void GoToPage()
        {
            driver.Navigate().GoToUrl(BASE_URL);
        }

        public DroppablePage OpenDroppable()
        {
            DroppableButton.Click();
            return new DroppablePage(driver);
        }
    }
}
