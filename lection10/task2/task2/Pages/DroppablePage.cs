﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;

namespace task2.Pages
{
    public class DroppablePage : BasePage
    {
        private MainPage mainPage;

        [FindsBy(How = How.XPath, Using = "//div[@id='droppable']")]
        public IWebElement DroppableSquere { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@id='draggable']")]
        public IWebElement DragSquere { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@id='droppable']//p")]
        public IWebElement DroppedText { get; set; }

        public DroppablePage(IWebDriver driver): base(driver)  { } 

        public void DragAndDrop()
        {
            mainPage = new MainPage(driver);
            mainPage.OpenDroppable();
            new Actions(driver).ClickAndHold(DragSquere)
                .MoveToElement(DroppableSquere)
                .Release().Perform();
        }
    }
}
