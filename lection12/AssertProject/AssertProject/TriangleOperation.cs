﻿
namespace AssertProject
{
    public class TriangleOperation
    {
        public int Perimeter(int a, int b, int z)
        {
           TriangleCheck triangleCheck = new TriangleCheck();
            if (triangleCheck.Cheking(a, b, z))
            {
                return a + b + z;
            }
            else throw new TriangleException("This isn't triangle");
        }
    }
}
