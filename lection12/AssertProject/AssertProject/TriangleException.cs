﻿using System;

namespace AssertProject
{
    public class TriangleException : Exception
    {
        public TriangleException(string message) : base(message){}
    }
}