using NUnit.Framework;
using System.Collections.Generic;

namespace AssertProject
{
    public class Test
    {
        private const int RESULT_PERIMERTER = 25;
        public TriangleCheck triangleCheck;
        public TriangleOperation triangleOperation;

        [SetUp]
        public void Setup()
        {
            triangleCheck = new TriangleCheck();
            triangleOperation = new TriangleOperation();
        }

        [Test]
        public void RightTriangle()
        {
            Assert.IsTrue(triangleCheck.Cheking(9, 10, 6), "This isn't triangle");
        }

        [Test]
        public void TruePerimeterTest()
        {
            Assert.AreEqual(RESULT_PERIMERTER, triangleOperation.Perimeter(9, 10, 6), "Perimeter is incorrect");
        }

        [Test]
        public void FalsePerimeterTest()
        {
            Assert.AreNotEqual(RESULT_PERIMERTER, triangleOperation.Perimeter(9, 10, 7), "Perimeter is correct");
        }

        [Test]
        public void OneOfTheSideIsMaxInt()
        {
            Assert.False(triangleCheck.Cheking(int.MaxValue, 6, 1), "This isn't triangle");
        }

        [Test]
        public void CompareNotSameObject()
        {
        TriangleOperation secondTriangleOperation = new TriangleOperation();
        Assert.AreNotSame(triangleOperation, secondTriangleOperation, "This objects are same");
        }

        [Test]
        public void CompareSameObject()
        {
            TriangleOperation secondTriangleOperation = triangleOperation;
            Assert.AreSame(triangleOperation, secondTriangleOperation, "This objects are same");
        }

        [Test]
        public void CompareTwoEqualNumber()
        {
           int firstNumber = 10;
           int secondNumber = 10;
           Assert.GreaterOrEqual(firstNumber, secondNumber, "The first number isn't equal or greater than the second number");
        }

        [Test]
        public void CompareTwoDifferentNumber()
        {
            int firstNumber = 12;
            int secondNumber = 10;
            Assert.Greater(firstNumber, secondNumber, "The first number isn't greater than the second number");
        }

        [Test]
        public void ContainsText()
        {
            string piece_text = "Hello";
            List<string> list = new List<string>() {"Hello"};
            Assert.Contains(piece_text, list, "This item isn't contained in list");
        }

        [Test]
        public void NullMessage()
        {
            string message = null;
            Assert.IsNull(message, "This message isn't null");
        }

        [Test]
        public void NotNullMessage()
        {
            string message = "Hello";
            Assert.IsNotNull(message, "This message null");
        }

        [Test]
        public void EmptyMessage()
        {
            string message = "";
            Assert.IsEmpty(message, "This message null");
        }

        [Test]
        public void NotEmptyMessage()
        {
            string message = "Hello";
            Assert.IsNotEmpty(message, "This message null");
        }
    }
}