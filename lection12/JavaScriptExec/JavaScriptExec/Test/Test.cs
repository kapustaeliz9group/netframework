using JavaScriptExec.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace JavaScriptExec
{
    [TestFixture]
    public class Test
    {
        private const string TITLE_SAP = "SAP & GitHub";
        private const string INPUT_TEXT = "WebDriver";
        private const int LOCATIN_UP = 0;
        private const int LOCATIN_DOWN = 5000;

        public IWebDriver Driver { get; set; }

        [SetUp]
        public void SetupTest()
        {
            Driver = new ChromeDriver();
            Driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void TearDown()
        {
            Driver.Quit();
        }

        [Test]
        public void JavaScriptExecTest()
        {
            GitHubPage gitHubPage = new GitHubPage(Driver);
            gitHubPage.ScrollDown();
            Assert.Less(LOCATIN_DOWN, Int64.Parse(gitHubPage.CheckLocationWindow()), "This isn't the end of the page");
            gitHubPage.ScrollUp();
            Assert.AreEqual(LOCATIN_UP, Int64.Parse(gitHubPage.CheckLocationWindow()), "This isn't the start of the page");
            SAPPage sAPPage = gitHubPage.OpenSAPPage();
            Assert.AreEqual(TITLE_SAP, sAPPage.TitleSAP.Text, "This isn't SAP page");
            sAPPage.Search();
            Assert.AreEqual(INPUT_TEXT, sAPPage.InputSearch.GetAttribute("value"), "WebDriver text haven't entered");
        }
    }
}
