﻿using JavaScriptExec.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace JavaScriptExec
{
    public class GitHubPage : BasePage
    {
        private const string BASE_URL = "https://github.com/";

        [FindsBy(How = How.XPath, Using = "//h1[contains(.,'SAP')]//..//span[contains(.,'Read more')]")]
        public IWebElement ButtonSAP { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='octicon octicon-logo-github']")]
        public IWebElement FooterLogo { get; set; }

        [FindsBy(How = How.XPath, Using = "//h1[@class='h000-mktg text-white lh-condensed-ultra mb-3']")]
        public IWebElement MainText { get; set; }

        public GitHubPage(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
            GoToPage(BASE_URL);
        }
        public void GoToPage(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public void ScrollDown()
        {
           ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0, document.body.scrollHeight);");
        }

        public string CheckLocationWindow()
        {
            string value =((IJavaScriptExecutor)driver).ExecuteScript("return window.pageYOffset;").ToString();
            return value;
        }

        public void ScrollUp()
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0, -document.body.scrollHeight);");
        }

        public SAPPage OpenSAPPage()
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].click();", ButtonSAP);
            return new SAPPage(driver);
        }
    }
}
