﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace JavaScriptExec.Pages
{
    public class SAPPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//h1[@class='h00-mktg lh-condensed my-3']")]
        public IWebElement TitleSAP { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Search GitHub']")]
        public IWebElement InputSearch { get; set; }

        public SAPPage(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }

        public void Search()
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("document.getElementsByClassName('input-sm header-search-input jump-to-field')[0].value='WebDriver'");
        }
    }
}
