﻿using System;
using System.Collections.Generic;

namespace Calculator
{
    class Program
    {
        private static double _result;
        private static double _firstNumber;
        private static double _secondNumber;
        readonly static string[] ValidInputOperation = new string[] { "1", "2"};
        readonly static string[] ValidInputNumber = new string[] { "1", "2", "3", "4" };
        readonly static string[] ValidAnswer = new string[] { "no", "yes" };
        private static List<string> _history = new List<string>();

        static void Main(string[] args)
        {
            Operation();
        }

        private static void OperationNumber()
        {
            string operation = null;
            while (true)
            {
                operation = GetMenuCalculate();
                if (ValidOperation(operation, ValidInputNumber))
                {
                    Calculate(operation);
                }
            }
        }

        private static void Operation()
        {
            string operation = null;
            while (true)
            {
                operation = GetMenu();
                if (ValidOperation(operation, ValidInputOperation))
                {
                    ChooseOperation(operation);
                }
            }
        }

        private static void OperationAnswerExit()
        {
            string answer = null;
            while (true)
            {
                answer = WantExitProgram();
                if (ValidOperation(answer, ValidAnswer))
                {
                    ExitProgram(answer);
                }
            }
        }

        private static void OperationAnswerZero()
        {
            string answer = null;
            while (true)
            {
                answer = WantResetZero();
                if (ValidOperation(answer, ValidAnswer))
                {
                    ResetZero(answer);
                }
            }
        }

        private static void OperationAnswerHistory()
        {
            string answer = null;
            while (true)
            {
                answer = WantSeeHistory();
                if (ValidOperation(answer, ValidAnswer))
                {
                    SeeHistory(answer);
                }
            }
        }

        private static bool ValidOperation(string operation, string[] valid)
        {
            bool isValid = false;
            foreach (string value in valid)
            {
                if (value == operation)
                {
                    isValid = true;
                    break;
                }
                else
                {
                    continue;
                }
            }
            return isValid;
        }

        private static string WantExitProgram()
        {
            Console.WriteLine("Do you want exit the program? yes/no");
            string answer = Console.ReadLine();
            return answer;
        }

        private static void ExitProgram(string answer)
        {
            switch (answer)
            {
                case "yes":
                    Environment.Exit(0);
                    break;
                case "no":
                    OperationAnswerZero();
                    break;
            }
        }
        private static string WantResetZero()
        {
            Console.WriteLine("Do you want to continue to work with the result? yes/no");
            string answer = Console.ReadLine();
            return answer;
        }

        private static void ResetZero(string answer)
        {
            switch (answer)
            {
                case "yes":
                    OperationNumber();
                    break;
                case "no":
                    _result = 0;
                    OperationNumber();
                    break;
            }

        }

        private static void PrintHistory()
        {
            foreach (var element in _history)
            {
                Console.Write(element);
            }
        }

        private static string WantSeeHistory()
        {
            Console.WriteLine("Do you want see the history?");
            string answer = Console.ReadLine();
            return answer;
        }

        private static void SeeHistory(string answer)
        {
            if (answer == "yes")
            {
                PrintHistory();
            }
            OperationAnswerExit();
        }

        private static void RepeatMatrix()
        {
            Console.WriteLine("Do you want muiltiply matrix? yes/no");
            string answer = Console.ReadLine();
            if (answer == "yes")
            {
                CalculateMatrix();
            }
        }
        private static void OperationAnswerExitMatrix()
        {
            string answer = null;
            while (true)
            {
                answer = WantExitProgram();
                if (ValidOperation(answer, ValidAnswer))
                {
                    ExitProgramMatrix(answer);
                }
            }
        }
        private static void ExitProgramMatrix(string answer)
        {
            switch (answer)
            {
                case "yes":
                    Environment.Exit(0);
                    break;
                case "no":
                    CalculateMatrix();
                    break;
            }
        }

        private static string GetMenu()
        {
            Console.WriteLine("Choose the operation: \n1. Calculate numbers\n2. Multiply matrix");
            string operation = Console.ReadLine();
            return operation;
        }

        private static void ChooseOperation(string operation)
        {
            if (operation == "1")
                OperationNumber();
            else
                CalculateMatrix();
        }

        private static string GetMenuCalculate()
        {
            Console.WriteLine("Choose the operation: \n1.Add\n2.Substract\n3.Multiply\n4.Divide");
            string operation = Console.ReadLine();
            return operation;
        }

        private static double CheckNumber(double number)
        {
            while (!double.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Exception : Enter the number");
            }
            return number;
        }

        private static void Calculate(string operation)
        {
            if (_result == 0)
            {
                Console.WriteLine("Enter the firstNumber");
                _firstNumber = CheckNumber(_firstNumber);
            }
            else
            {
                _firstNumber = _result;
            }
            Console.WriteLine("Enter the secondNumber");
            _secondNumber = CheckNumber(_secondNumber);
            MenuOperation(operation);
        }

        private static void MenuOperation(string operation)
        {
            _history.Add(Convert.ToString(_firstNumber));

            switch (operation)
            {
                case "1":
                    _result = Add(_firstNumber, _secondNumber);
                    _history.Add("+");
                    break;
                case "2":
                    _result = Substract(_firstNumber, _secondNumber);
                    _history.Add( "-");
                    break;
                case "3":
                    _result = Multiply(_firstNumber, _secondNumber);
                    _history.Add("*");
                    break;
                case "4":
                    _result = Divide(_firstNumber, _secondNumber);
                    _history.Add("/");
                    break;
            }

            Console.WriteLine($"Result: {_result}");

            _history.Add(Convert.ToString(_secondNumber));
            _history.Add("=");
            _history.Add(Convert.ToString(_result + "\n"));


            OperationAnswerHistory();
        }

        private static double Add(double firstNumber, double secondNumber) => firstNumber + secondNumber;

        private static double Substract(double firstNumber, double secondNumber) => firstNumber - secondNumber;

        private static double Multiply(double firstNumber, double secondNumber) => firstNumber * secondNumber;

        private static double Divide(double firstNumber, double secondNumber)
        {
            if (secondNumber == 0)
            {
                Console.WriteLine("Can not be divided by zero");
                OperationNumber();
            }
            return firstNumber / secondNumber;
        }

        private static void CalculateMatrix()
        {
            var firstMatrix = GetMatrixFromConsole("first matrix");
            var secondMatrix = GetMatrixFromConsole("second matrix");

            Console.WriteLine("The first matrix:");
            PrintMatrix(firstMatrix);

            Console.WriteLine("The second matrix:");
            PrintMatrix(secondMatrix);

            Console.WriteLine("Matrix Product:");
            MatrixMultiplication(firstMatrix, secondMatrix);
            OperationAnswerExitMatrix();
        }

        private static void MatrixMultiplication(int[,] matrixA, int[,] matrixB)
        {
            if (matrixA.ColumnsCount() == matrixB.RowsCount())
            {
                var matrixC = new int[matrixA.RowsCount(), matrixB.ColumnsCount()];

                for (var i = 0; i < matrixA.RowsCount(); i++)
                {
                    for (var j = 0; j < matrixB.ColumnsCount(); j++)
                    {
                        matrixC[i, j] = 0;

                        for (var k = 0; k < matrixA.ColumnsCount(); k++)
                        {
                            matrixC[i, j] += matrixA[i, k] * matrixB[k, j];
                        }
                    }
                }
                PrintMatrix(matrixC);
            }
            else
            {
                Console.WriteLine("Multiplication is not possible! The number of columns in the first matrix is not equal to the number of rows in the second matrix.");
            }
            
        }

        private static void PrintMatrix(int[,] matrix)
        {
            for (var i = 0; i < matrix.RowsCount(); i++)
            {

                for (var j = 0; j < matrix.ColumnsCount(); j++)
                {
                    Console.Write(matrix[i, j] + "  ");
                }

                Console.WriteLine();
            }
        }

        private static int[,] GetMatrixFromConsole(string name)
        {
            Random matrixRandom = new Random();

            Console.Write("The number of rows of the {0}: ", name);
            var n = int.Parse(Console.ReadLine());
            Console.Write("The number of column of the {0}: ", name);
            var m = int.Parse(Console.ReadLine());

            var matrix = new int[n, m];
            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < m; j++)
                {
                    Console.Write("{0}[{1},{2}] = ", name, i, j);
                    matrix[i, j] = int.Parse(Console.ReadLine());
                }
            }
            return matrix;
        }
    }
}
